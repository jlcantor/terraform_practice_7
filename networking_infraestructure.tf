resource "aws_subnet" "intsub1" {
  vpc_id     = "${data.aws_vpc.default.id}"
  cidr_block = "172.31.41.0/24"

  tags {
    Name = "jose.cantor"
  }
}

resource "aws_subnet" "intsub2" {
  vpc_id     = "${data.aws_vpc.default.id}"
  cidr_block = "172.31.42.0/24"

  tags {
    Name = "jose.cantor"
  }
}

resource "aws_subnet" "extsub1" {
  vpc_id     = "${data.aws_vpc.default.id}"
  cidr_block = "172.31.43.0/24"

  tags {
    Name = "jose.cantor"
  }
}

resource "aws_subnet" "extsub2" {
  vpc_id     = "${data.aws_vpc.default.id}"
  cidr_block = "172.31.44.0/24"

  tags {
    Name = "jose.cantor"
  }
}

resource "aws_network_acl" "intsub1" {
  vpc_id = "${data.aws_vpc.default.id}"
  subnet_id = "${aws_subnet.intsub1.id}"

  egress {
    protocol   = "all"
    rule_no    = 101
    action     = "deny"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "all"
    rule_no    = 102
    action     = "deny"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = "all"
    rule_no    = 103
    action     = "allow"
    cidr_block = "${aws_subnet.extsub1.cidr_block}"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "all"
    rule_no    = 104
    action     = "allow"
    cidr_block = "${aws_subnet.extsub1.cidr_block}"
    from_port  = 0
    to_port    = 0
  }

  tags {
    Name = "jose.cantor"
  }
}

resource "aws_network_acl" "intsub2" {
  vpc_id = "${data.aws_vpc.default.id}"
  subnet_id = "${aws_subnet.intsub2.id}"

  egress {
    protocol   = "all"
    rule_no    = 201
    action     = "deny"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "all"
    rule_no    = 202
    action     = "deny"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = "all"
    rule_no    = 203
    action     = "allow"
    cidr_block = "${aws_subnet.extsub2.cidr_block}"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "all"
    rule_no    = 204
    action     = "allow"
    cidr_block = "${aws_subnet.extsub2.cidr_block}"
    from_port  = 0
    to_port    = 0
  }

  tags {
    Name = "jose.cantor"
  }
}